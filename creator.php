<?php /*
Coniunctio
Copyright (C) 2020 Felix v. Oertzen
coniunctio@von-oertzen-berlin.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
$NEVER = "9999-09-09";

$db = new PDO('sqlite:shortlinkdatabase.db');
$createshortl = isset($_POST['createshortlink']);
$userauth = false;
$linkexistsalready = false;

if ($createshortl) {
  sleep(1);
  $pwd = $db->query("SELECT shortlink FROM shortlinks WHERE task = 0")->fetch()['shortlink'];
  if (password_verify($_POST['secret'], $pwd)) {
    $userauth = true;

    $le = $db->prepare("SELECT task FROM shortlinks WHERE shortlink = ?");
    $shortlink = htmlentities($_POST['shortlink']);
    $le->bindParam(1, $shortlink);
    $le->execute();
    $linkexistsalready = $le->fetchAll();
    if (!$linkexistsalready){
      $ie = $db->prepare("INSERT INTO shortlinks (shortlink, destination, creation, expiration, ip, host, useragent, remaining) VALUES (:shrtlnk, :dstntn, :crtn, :exprtn, :ip, :host, :useragent, :remaining)");
      $dstntn = "https://".htmlentities($_POST['longlink']);
      $crtn   = date("Y-m-d");
      $exprtn = $_POST['expiration'];
      $ip     = $_SERVER['REMOTE_ADDR'];
      $host   = ($_SERVER['REMOTE_HOST'] ? $_SERVER['REMOTE_HOST'] : "no hostname found");
      $ua     = $_SERVER['HTTP_USER_AGENT'];
      $remn   = htmlentities($_POST['usages']);  
      $ie->bindParam(':shrtlnk',   $shortlink);
      $ie->bindParam(':dstntn',    $dstntn);
      $ie->bindParam(':crtn',      $crtn);
      $ie->bindParam(':exprtn',    $exprtn);
      $ie->bindParam(':ip',        $ip);
      $ie->bindParam(':host',      $host);
      $ie->bindParam(':useragent', $ua);
      $ie->bindParam(':remaining', $remn);
      $ie->execute();
    }
  }
}
?><!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Coniunctio</title>
    <link href="styles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="moment.min.js"></script>
    <script>
    function never(){
      exp = document.getElementById("expiration");
      exp.value = "<?php echo $NEVER;?>";
    }
    function snackbar(snacktype) {
      var x = document.getElementById("snackbar_" + snacktype);
      x.className = x.className + " show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 6000);
    }
    function fallbackCopyTextToClipboard(text) {
      var textArea = document.createElement("textarea");
      textArea.value = text;
      textArea.style.top = "0";
      textArea.style.left = "0";
      textArea.style.position = "fixed";
      document.body.appendChild(textArea);
      textArea.focus();
      textArea.select();

      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Fallback: Copying text command was ' + msg);
      } catch (err) {
        console.error('Fallback: Oops, unable to copy', err);
      }

      document.body.removeChild(textArea);
    }
    function copyTextToClipboard(text) {
      snackbar("copy");
      if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text);
        return;
      }
      navigator.clipboard.writeText(text).then(function() {
        console.log('Async: Copying to clipboard was successful!');
      }, function(err) {
        console.error('Async: Could not copy text: ', err);
      });
    }
    function checkLogin() {
      <?php echo $createshortl && !$userauth ? 'snackbar("auth_failed");': '';?>
      <?php echo $linkexistsalready ?  'snackbar("link_already_there");': '';?>
    }
    </script>
  </head>
  <body style="font-family:San Francisco, Helvetica Neue, Arial;padding: 16px;" onpageshow="checkLogin()">
    <h2>Coniunctio</h2>
    <?php echo $_SERVER['HTTPS'] ? "":"<h3 style='color:darkred; padding:8px;border: 4px double darkred;'>You reached this page without encryption. It's much safer to use encryption in order to create shortlinks.</h3>"; ?>
    <!-- DIVISION HERE -->
    <div class="container" style="<?php if (!($userauth && $createshortl && !$linkexistsalready)){?>display:none;<?php }?>">
      <div class="row">
        <h3>your short link</h3>
      </div>
      <div class="row">
        <div class="col-25">
          <label>Your short link reads as follows:</label>
        </div>
        <div class="col-75">
          <span>https://<?php echo $_SERVER['HTTP_HOST'];?>/<?php echo htmlentities($_POST['shortlink']);?></span>
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label>The original link was:</label>
        </div>
        <div class="col-75">
          <span>https://<?php echo htmlentities($_POST['longlink']);?></span>
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label>It will expire on:</label>
        </div>
        <div class="col-75">
          <span><?php if (!(htmlentities($_POST['expiration']) == $NEVER)) {?>
            <script>document.write(moment("<?php echo htmlentities($_POST['expiration']);?>", "YYYY-MM-DD").format("LL"));</script>
          <?php } else { ?>never<?php }?>
          </span>
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label>It will be usable for:</label>
        </div>
        <div class="col-75">
          <span><?php if ($_POST['usages'] == -1) {echo "endless times";} else {echo htmlentities($_POST['usages']); if ($_POST['usages'] > 1) {?> times<?php } else {?> time<?php }}?></span>
        </div>
      </div>
      <div class="row">
        <input type="button" value="copy clipboard" style="float:right;" onclick="copyTextToClipboard('https://<?php echo $_SERVER['HTTP_HOST'];?>/<?php echo htmlentities($_POST['shortlink']);?>')">
      </div>
    </div>
  </div>
<!-- DIVISION HERE -->
    <form id="shortlinkform" class="container" action="<?php echo $_SERVER['SCRIPT_NAME'];?>" method="post">
      <div class="row">
        <h3>create a new shortlink</h3>
      </div>
      <div class="row">
        <div class="col-25">
          <label for="shortlink">short link</label>
        </div>
        <div class="col-75">
          https://<?php echo $_SERVER['HTTP_HOST'];?>/ <input type="text" id="shortlink" required name="shortlink" placeholder="How should we call the link?" value="<?php echo htmlentities($_POST['shortlink']);?>">
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label for="longlink">original URL</label>
        </div>
        <div class="col-75">
          https://<input type="text" id="longlink" required name="longlink" placeholder="What's the URL you wanna shorten?" value="<?php echo htmlentities($_POST['longlink']);?>">
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label for="expiration">Expiration</label>
        </div>
        <div class="col-75">
          <input type="date" id="expiration" required name="expiration" placeholder="When should we destroy your link?" value="<?php echo htmlentities($_POST['expiration']);?>">
          <input type="button" value="never" onclick="never()">
        </div>
      </div>
      <div class="row" style="display:flex">
        <div class="col-25">
          <label for="usages">Number of usages</label>
        </div>
        <div class="col-75" display="vertical-align: middle;">
          <select name="usages" id="usages">
            <option value="-1">unlimited usages</option>
            <option value="1">1 usage</option>
            <?php for ($n=2; $n<60+1; $n++) { ?>
            <option value="<?php echo $n;?>"><?php echo $n;?> usages</option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label for="secret">Secret</label>
        </div>
        <div class="col-75">
          <input type="password" id="secret"  name="secret" required placeholder="This secret empowers you to use this service! If you know it...">
        </div>
      </div>
      <div class="row">
        <input type="hidden" name="createshortlink" value="yes"/>
        <input type="submit" value="save"/>
      </div>
    </form>
    <div id="snackbar_copy" class="snackbar">link copied to clipboard</div>
    <div id="snackbar_auth_failed" class="snackbar">You don't know the secret.<br>How dare you?</div>
    <div id="snackbar_link_already_there" class="snackbar">This shortlink already exits.<br>please choose a different one.</div>
    <?php
    require_once("body.include"); footer();?>
  </body>
</html>
