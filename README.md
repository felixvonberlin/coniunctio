![License](https://img.shields.io/badge/License-GNU%20Affero%20General%20Public%20License%20v3.0-green.svg?logo=gnu)

![HTML5](https://img.shields.io/badge/uses-HTML5-red.svg?logo=html5&color=e34f26) ![CSS](https://img.shields.io/badge/uses-CSS3-blue.svg?logo=css3&color=1572b6) ![JavaScript](https://img.shields.io/badge/uses-JavaScript-yellow.svg?logo=javascript&color=f7df1e)
# Coniunctio
This is a quite simple project which allows you to run your own short link service.
You are able to
* create personal links
* set a expiration date
* limit the count of usages.

## requirements
You will need
* a web space with 10MB free storage
* a php instance which is able to deal with SQlite and PDO
* 10 minutes time.

## installation
Just put these files on your web space and open the `index.php` page.

You will see a page which reveals a secret code in order to confirm that only you are able to create new shortlinks.

#### attention
At this point of time it is essential that this service is hosted on its own domain.
Obvioulsy a subdomain is enough.

I'd recommend something like `s.myowndomain.eu` or `short.myowndomain.eu`...

# third party libraries
This project includes
* moment.js written by [JS Foundation and other contributors](https://github.com/moment/moment/) under [MIT License.](https://github.com/moment/moment/blob/develop/LICENSE)


# nginx config
```
    location /style.css {
      index style.css;
    }

    location ~* /creator.php$ {
        fastcgi_pass    unix:/var/run/php/php-fpm.sock;
        include         fastcgi_params;
        fastcgi_param   SCRIPT_FILENAME    $document_root$fastcgi_script_name;
        fastcgi_param   SCRIPT_NAME        $fastcgi_script_name;
    }
    location / {
        fastcgi_index   index.php;
        fastcgi_pass    unix:/var/run/php/php-fpm.sock;
        include         fastcgi_params;
        fastcgi_param   SCRIPT_FILENAME    "$document_root/index.php";
   }

```
